def ground_shipping(weight):
  flat = 20.00
  
  if weight <= 2:
    cost = flat + 1.50 * weight
  if (weight > 2) and (weight <= 6):
    cost = flat + weight * 3.00
  if (weight > 6) and (weight <= 10):
    cost = flat + weight * 4.00
  if (weight > 10):
    cost = flat + weight * 4.75
    
  return cost

premium_ground_shipping = 125.00

print(premium_ground_shipping(8.4))
