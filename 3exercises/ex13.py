from sys import argv # From sys module, grab the argv feature.
script, first, second, third, itsaboy = argv # Mapping command line args. $0, $1, $2 $3 name of script, 3 variables
first = input("You are a cow. Tell me why.")
print("The script is called:", script)
print("Your first variable is:", first)
print("Your second variables is:", second)
print("Your third variable is:", third)
print("Your fourth variable is:", itsaboy)

print(">>> argv=", repr(argv))
# You can break it by not putting the right amount of variables.

hi = input("You are a cow. Tell me why.")
print(hi)
