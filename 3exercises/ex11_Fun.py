# I had some fun with this one. If someone enters ' instead of '' for inches it will proclaim their immense height.

shortInches = False # set shortInches to False

#print("How old are you?", end=' ')
#age = input()
height = input("How tall are you? Please format your height like 5'9 :")
#print("How much do you weigh?", end=' ')
#weight = input()

#print(f"So, you're {age} old, {height} tall and {weight} heavy.")
first = height[0] # Get first letter of the height variable.
second = height[1] # Get second letter of the height variable.
third = height[2] # Get third letter of the height variable.
last = height[-1] # Get last letter of the height variable.

if height[2] == height[-1]: # If the third and last letters of height are the same, set shortInches to true.
    shortInches = True

#like = input("Do you like this?")
#print(like)
#1. Done.
#2 & #3 That was a lot of fun.

if shortInches == True: # If shortInches is true, then print this
    print("You are " + first + " feet " + third + " inches tall.")
else: # Otherwise, print this
    print("You are " + first + " feet " + third + last + " inches tall.")

if "\"" in height: # This is good fun. If the user uses inches instead of feet by accident, let them know sarcastically.
    print("Wow! You are " + height[0] + " inches tall. That's SO TALL!")

if not "\"" in height: # Otherwise, it's not that interesting. if not
    print("A boring normal height.")
