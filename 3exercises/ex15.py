 # Import the argv tool
from sys import argv
import os.path

# Grab two arguments wtih argv
script, filename = argv

if not os.path.isfile(filename): # If file doesn't exist, create it.
    with open(filename, 'w') as f: # Write the following string to it.
        print(" NEW FILE!\n ONLY 99 CENTS!\n Just kidding, it's free.\n Welcome to the galaxy.", file=f)

        #Close the file.
        filename.close()

# set the variable txt to the open command for the file taken as input.

txt = open(filename)
#txt.close() #Break stuff

# Tells user that their file of the filename {filename} is going to be printed.
print(f"Here's your file {filename}:")

# Print file
print(txt.read())

# Close the file
txt.close()

# Asks them to input the name again.
print("Type the filename again:")

# Take input on a prompt.
file_again = input("> ")

# Take input on a prompt.
txt_again = open(file_again)

# Read the file and print.
print(txt_again.read())

txt.close()

#1-4 Done.
#5. Getting through argv lets you run operations on it throughout the script without having to wait for input.
