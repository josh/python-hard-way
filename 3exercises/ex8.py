formatter = "{} {} {} {} {}" # Create 4 slots to fill.

print(formatter.format(1, 2, 3, 4, 5)) # Fill them with 1, 2, 3, 4
print(formatter.format("one", "two", "three", "four", "five")) # Spell it out.
print(formatter.format(True, False, False, True, False)) # True, false, false, true
print(formatter.format(formatter, formatter, formatter, formatter, formatter)) # Print itself 4 times.
print(formatter.format(

    "Try your",
    "Own text here",
    "Maybe a poem",
    "Or a song about fear",
    "Or a song of redemption."
)) # One long string, don't forget the commas.
