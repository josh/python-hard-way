# Import the argv tool from system tools.
from sys import argv

# Grab some command line arguments with argv.
script, input_file = argv


def print_all(f): # Define a function with one argument.
    print(f.read()) # Read an a file passed to this function.

def rewind(f): # Define a function with one argument.
    f.seek(0) # Start from the first line.

def print_a_line(line_count, f): # Define a function with two arguments.
    print(line_count, f.readline()) # Print the line.

current_file = open(input_file) # Open the file input_file as "current_file"

print("First let's print the whole file:\n") # Tell the user we're printing the whole file.

print_all(current_file) # Print the whole file.

print("Now let's rewind, kind of like a tape.") # Tell the user we're starting over.

rewind(current_file) # Print the first line.

print("Let's print three lines:") # Tell the user we're printing three lines.

current_line = 1 # Set the current line to the second line.
print_a_line(current_line, current_file) # Print the current line.

current_line = current_line + 1 # Increment the current line by one, making it the third line.
print_a_line(current_line, current_file) # Print the current line.

current_line = current_line + 1 # Increment the current line by one, making it the fourth line.
print_a_line(current_line, current_file) # Print the current line.
