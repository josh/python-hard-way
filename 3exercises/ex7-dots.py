print("Mary had a little lamb.") # Print string
print("Its fleece was white as {}.".format('snow')) # Print string with format.
print("And everywhere that Mary went.") # Print string.

with open('out.txt', 'w') as f:
    print("." * 1000000000, file=f)  # A little fun with a billion dots being saved to a file.

end1 = "C" # Set variable to a string.
end2 = "h" # Set variable to a string.
end3 = "e" # Set variable to a string.
end4 = "e" # Set variable to a string.
end5 = "s" # Set variable to a string.
end6 = "e" # Set variable to a string.
end7 = "B" # Set variable to a string.
end8 = "u" # Set variable to a string.
end9 = "r" # Set variable to a string.
end10 = "g" # Set variable to a string.
end11 = "e" # Set variable to a string.
end12 = "r" # Set variable to a string.


# watch end = ' ' at the end. try removing it to see what happens.
print(end1 + end2 + end3 + end4 + end5 + end6, end=' ') # Print combined variables without a space in between on the same line as the next set of them with a space in between:
print(end7 + end8 + end9 + end10 + end11 + end12) # Print variables without spaces in between

#1. Done
#2. Done

# Break it.
