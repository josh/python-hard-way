
age = int(input("How old are you?" ))
#age = print("How old are you?" , input()) # It's asking for input before prompting the user.
height = input(f"You're {age}? Nice. How tall are you? ")
weight = input("How much do you weigh? ")

print(f"So, you're {age} old, {height} tall and {weight} heavy.")

# {age} printed in absence of f

# open() allows you to open files in this syntax:
#open(name[, mode[, buffering]]) -> file object

# Likea so: with open('out.txt', 'w') as f: # W = write f = var to be used later.
#    print("." * 1000000000, file=f)  # A little fun with a billion dots being saved to a file.

#file(name[, mode[, buffering]]) -> file object
#
# Pydoc:
# Open a file.  The mode can be 'r', 'w' or 'a' for reading (default),
# |  writing or appending.  The file will be created if it doesn't exist
# |  when opened for writing or appending; it will be truncated when
# |  opened for writing.  Add a 'b' to the mode for binary files.
# |  Add a '+' to the mode to allow simultaneous reading and writing.
# |  If the buffering argument is given, 0 means unbuffered, 1 means line
# |  buffered, and larger numbers specify the buffer size.  The preferred way
# |  to open a file is with the builtin open() function.
# |  Add a 'U' to mode to open the file for input with universal newline
# |  support.  Any line ending in the input file will be seen as a '\n'
# |  in Python.  Also, a file so opened gains the attribute 'newlines';
# |  the value for this attribute is one of None (no newline read yet),
# |  '\r', '\n', '\r\n' or a tuple containing all the newline types seen.
# |
# |  'U' cannot be combined with 'w' or '+' mode.

# os.curdir is a string representing the current directory ('.' or ':')
# os.pardir is a string representing the parent directory ('..' or '::')
# - os.pathsep is the component separator used in $PATH etc
# - os.linesep is the line separator in text files ('\r' or '\n' or '\r\n')
