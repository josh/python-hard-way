tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = '''
I'll do a list:
\t* Cat food
\t* Fishes
\t* Catnip\n\t* Grass
\t* Yo \a Yo\bcow\rwhat\vcow
\U0001F60D
'''

print(tabby_cat)
print(persian_cat)
print(backslash_cat)
print(fat_cat)

# 1. Got them on a study sheet - hex, others not explained.
# 2. don't know
# 3. Done. See fat_cat
