# Here's some new strange stuff, remember type it exactly.

Cows = "Fred Mary Freddy Faith Freddy Jr. Felicity"
Dogs = "\n\nCharlie\nChocolate\nGeorge\nCharlie Jr.\nTerri\nFifi\nGoldie\nTimmy\nTabby"

print("Here are the dogs: ", Dogs)
print("\nHere are the cows: ", Cows)

print("""
    But who likes Stubbly Grass Nibbles?

    Why, Toby of course.
    And Blueby is obsessed. The shopping is strong with this one.
    Toby Jr. would rather invent a Stubbly Grass Generator.
    Bubbles loves clothes, but refuses to go on the ridiculous shopping sprees \n    of her mother.
    """)
