#!flask/bin/python
from flask import Flask, request, render_template
import os
import subprocess
import re

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template('my-form.html')

@app.route('/', methods=['POST'])
def form_post():
    text = request.form['secret']
    text = re.sub (r'([^a-zA-Z]+?)', '', text) # Remove all non-letters.
    command = "python3 Caeser-Cipher/Caeser-Cipher.py encrypt " + str(text) + " 3"
    ciphered_text = subprocess.check_output(command, shell=True)
    return ciphered_text

if __name__ == '__main__':
    app.run(host= '0.0.0.0')
