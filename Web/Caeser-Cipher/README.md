# Python Caeser Cipher Helper

# What's That?

This script gives you tools to manipulate text with the Caeser Cipher.
It'll take any input you give it, filter it and process it as you want (encrypt or decrypt) and will attempt to crack it if you don't know the shift.

This is a fun tool, not security. The Caeser Cipher was broken in A.D. 800 (or earlier) for longer messages unless you use a one time pad and a randomly generated shift. But, it's fun to break and manipulate.

This tool is a work in progress.

# Requirements

  Python `=>` 3.5.2

# Usage

### Encrypt

`python3 caeser-cipher.py encrypt [secretmessage] [shift]`

For example: `python3 caeser-cipher.py encrypt "Hi, how are you?" 3"`

### Decrypt

`python3 caeser-cipher.py decrypt "encryptedtext"`

For example: `python3 caeser-cipher.py decrypt "hhhhhhhhhh" 3`

If you don't know the shift for the encrypted text, use 0 for the shift. The script will attempt to break the cipher for you.
