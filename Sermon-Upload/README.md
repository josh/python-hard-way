# Sermon Upload

Finds latest sermon based on input, uploads it, notifies the user whether or not it works. Sounds simple, it isn't.

# Requirements

Install `notify2` and `dbus-python` and other dependencies:

```
sudo apt-get install libgtk2.0-dev libdbus-1-dev
```
```
pip install dbus-python notify2
```
