while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    --srcdir)
    srcdir="$2"
    shift # past argument
    ;;
    --dstdir)
    dstdir="$2"
    shift # past argument
    ;;
    --key)
    key="$2"
    shift # past argument
    ;;
    --ssh)
    ssh="$2"
    shift # past argument
    ;;
    --file)
    file="$2"
    shift # past argument
    ;;
    *)
    # unknown option
    if test -z "${unknown}"
    then
      unknown=$1
    else
      echo "Unknown Option"
      exit 1
    fi
    ;;
  esac
  shift # past argument or value
done


cd $dstdir
sermons=$(ls $srcdir)
IFS=$'\n'
lastsermon=$(echo "${sermons[*]}" | sort -nr | head -n1)
cp $srcdir/$lastsermon .

rsync --update --progress -e "ssh -i $key" *.mp3 $ssh:$dstdir

sermons=$(ssh -i $key $ssh 'ls $dstdir')
IFS=$'\n'
recentsermon=$(echo "${sermons[*]}" | sort -nr) #| head -n2)
echo $recentsermon
