# Sermon Upload v2.1 Alpha

import subprocess
from subprocess import call # We can do BASH in Python if needed.
import os # Lots of system stuff
from shutil import copyfile # Allow copying of files
import glob # Import glob tools for file listings.
import notify2 # Notification utility.
import time # Import time into the galaxy
import re
import argparse

parser = argparse.ArgumentParser(usage="python3 sermon-upload.py source destination keyfile sshinfo \nFor example, python3 sermon-upload.py /home/josh/Downloads/Downloads/ /home/josh/Downloads/Downloads2/ /home/josh/.ssh/id_rsa josh@127.0.0.1")
parser.add_argument("scrdir")
parser.add_argument("dstdir")
parser.add_argument("key")
parser.add_argument("ssh")
args = parser.parse_args()

def upload(scrdir, dstdir, key, ssh): # Input has to end in /
    # time.sleep(10) # Wait 10 seconds to make sure the machine is booted if we're doing auto. NOTE: Change to only run when auto is specified.
    os.chdir(scrdir) # Go to sermon directory.
    list = sorted(glob.glob('*.mp3')) # Sort files alphabetically.
    mostrecent = list[-1] # Find most recent sermon
    secondrecent = list[-2] # Find second most recent sermon
    #print(mostrecent)
    #mostrecent = re.sub (' ', '\\', mostrecent) # NOTE to self: figure out a way to handle spaces.
    #print(mostrecent)
    sermonsrc = scrdir + mostrecent # Grab the most recent sermon to copy.
    sermondst = dstdir + mostrecent # Destination for most recent sermon.

    copyfile(sermonsrc, sermondst) # Copy the sermon

    os.chdir(dstdir) # Go to the directory where the sermon has been copied to

    # Export some variables to BASH

    # os.popen("export sermondst")
    os.environ['sermondst'] = sermondst
    # os.popen("export key")
    os.environ['key'] = key
    # os.popen("export ssh")
    os.environ['ssh'] = ssh
    os.popen('rsync --update --progress -e "ssh -i $key" $mostrecent $ssh:$sermondst') # Upload the sermon.
    # os.popen("export dstdir")
    os.environ['dstdir'] = dstdir
    # os.popen("export recentsermon") # Export the list to Python
    recentsermon = os.popen('ssh -i $key $ssh ls $sermondst').read() # Get most recent sermon from destination server.

    # recentsermon = recentsermon[-1:]
    recentsermon = recentsermon.rstrip() # Strip whitespace at the end of the string.
    # recentsermon = recentsermon.split(',')
    # print(recentsermon[-1])
    # recentsermon = recentsermon[-1]
    # recentsermon = "".join(recentsermon.split()) # Strip whitespace.

    #print(mostrecent)
    #print(recentsermon)

    if recentsermon == sermondst: # NOTE: to self: add option to skip notification for no dependencies

    # if str(mostrecent) == str(recentsermon): # If the most recent sermon uploaded matches the most recent sermon locally, we're good.

        notify2.init('Sermon Script') # Notify user of success.
        n = notify2.Notification("Sermon Uploaded",
             mostrecent
                 )
        n.show()

    else: # Otherwise, something went wrong, say it failed.

         notify2.init('Sermon Script')
         n = notify2.Notification("Sermon Upload Failed",
              mostrecent
             )
         n.show()

upload(args.scrdir, args.dstdir, args.key, args.ssh)
#upload("/mnt/52265C99265C8041/Users/Berean-Sound/sermons", "/home/josh/sermons/2018", "/home/berean/sermons/.tempssh/temp", "sermons@bereanbibleutah.org")
