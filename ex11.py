#Write your function here

def every_three_nums(start):
  if start > 100:
      return []

  everythree = range(start, 101, 3)
  return list(everythree)

#Uncomment the line below when your function is done
print(every_three_nums(91))
