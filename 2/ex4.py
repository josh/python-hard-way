# Number of cars
cars = 100
# Number of people that can be fit in each car.
space_in_a_car = 4.0
# Number of people driving.
drivers = 30
# Number of passengers riding.
passengers = 90
# Number of cars not driven.
cars_not_driven = cars - drivers
# Number of cars driven.
cars_driven = drivers
# The total number of people that can ride in each car.
carpool_capacity = cars_driven * space_in_a_car
# The average number of passengers in each car.
average_passengers_per_car = passengers / cars_driven # Line number 8, the first time, he tried to do car_pool_capacity instead of carpool_capacity

print "There are", cars, "cars available."
print "There are only", drivers, "drivers available."
print "There will be", cars_not_driven, "empty cars today."
print "We have", passengers, "to carpool today."
print "We need to put about", average_passengers_per_car, "in each car."

# 1. I used 4.0 for space_in_a_car, but is that necessary? What happens if it's just 4? Nothing changes in this case.
# 2. Remember that 4.0 is a floating point number. It's just a number with a decimal point, and you need 4.0 instead of just 4 so that it is floating point.
# 3. Write comments above each of the variable assignments.
# 4-6 completed as well.
