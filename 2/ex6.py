x = "There are %d types of people." % 10 # Create a variable that is a string with a variable of %d which is equal to 10.
binary = "binary" # Create variable that is a string.
do_not = "don't"  # Create variable that is a string.
y = "Those who know %s and those who %s." % (binary, do_not) # %s and %s correspond to binary and do_not

print x # Print variable x
print y # Print variable y

print "I also said: %r." % x #2: First # Print a string with a variable.
print "I also said: '%s'." % y #2: Second and third # Print a string with the string variable y

hilarious = False # Set variable hilarious to False
joke_evaluation = "Isn't that joke so funny?! %r" #2: Fourth # Set a variable to a string with a variable yet to be referenced.

print joke_evaluation % hilarious # print a variable and another variable (referring back to %r)

w = "This is the left side of..." # A string corresponding to the left side of a bigger string.
e = "a string with a right side." # A string corresponding to the right side of a bigger string.

print w + e # Add the strings together.

#1. Done.
#2. Done
#3. You called y (2 strings in a string) and x (1 string in a string) and you called joke_evaluation (line 13 & 15) which has another string in a string for a total of 4self.
#4. 
