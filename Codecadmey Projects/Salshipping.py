premium_ground_shipping = 125.00

def ground_shipping(weight):
  flat = 20.00
  
  if weight <= 2:
    cost = flat + 1.50 * weight
  if (weight > 2) and (weight <= 6):
    cost = flat + weight * 3.00
  if (weight > 6) and (weight <= 10):
    cost = flat + weight * 4.00
  if (weight > 10):
    cost = flat + weight * 4.75
    
  return cost

def drone_shipping(weight):
  if (weight <= 2):
    cost = weight * 4.50
  elif (weight > 2) and (weight <= 6):
    cost = weight * 9.00
  elif (weight > 6) and (weight >= 10):
    cost = weight * 12.00
  else:
    cost = weight * 14.25
    
  return cost

def shipping_cost(weight):
  ground = ground_shipping(weight)
  drone = drone_shipping(weight)
  
  if ground < premium_ground_shipping and ground < drone:
    cost = ground_shipping(weight)
    return "Ground Shipping is the cheapest option! It will cost: $%s" % cost
  
  elif drone < premium_ground_shipping and drone < ground:
    cost = drone_shipping(weight)
    return "Drone Shipping is the cheapest option! It will cost $%s" % cost
  
  elif premium_ground_shipping < ground and premium_ground_shipping < drone:
    cost = premium_ground_shipping
    return "Premium Ground Shipping is the cheapest option! It will cost $%s" % cost
    
print(shipping_cost(1.5))
print(shipping_cost(4.8))
print(shipping_cost(41.5))
