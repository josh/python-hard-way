

```
def function1(arg):

    print(arg)

def function2(argument):

    print(argument)
    argument = argument + ". HI!"
    function1(argument)

function2("Hello World")
```

So, what are we doing? We're giving function2 an argument that it prints, modifies and sends it to function1. Function1 then prints the new argument that was passed to it by funciton2 and we're done. If you have any questions, hit me up in the comments.
