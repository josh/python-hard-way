Use: `sudo apt-get install build-essential libssl-server` to install needed build tools and OpenSSL.

Download the source for Python 3.6.6: [https://www.python.org/ftp/python/3.6.6/Python-3.6.6.tgz](https://www.python.org/ftp/python/3.6.6/Python-3.6.6.tgz)

Then go into the directory where you downloaded python, extract it.

Open a terminal in that folder.

Run:

`./configure --enable-optimizations --prefix=/opt/phw/`

to install Python with stable optimizations (good performance, stable) in the directory `/opt/phw`

There's some weird issues with filesystem paths so we need to create a BASH script:

`sudo nano /usr/local/bin/phw`

Put the following in the file to make it excecute the commands properly to the Python install:

```
#!/bin/bash

/opt/phw/bin/python3.6 "$@"
```


