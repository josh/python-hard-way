#!flask/bin/python
from flask import Flask, request, render_template
import os
import subprocess
import re

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template('my-form.html')

@app.route('/', methods=['POST'])
def form_post():
    command = "python apol-choice.py"
    topics = subprocess.check_output(command, shell=True)
    return topics

if __name__ == '__main__':
    app.run(host= '0.0.0.0')
