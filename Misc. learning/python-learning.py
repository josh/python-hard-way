'''
On line #4 Regex
This feeds a regex to re.sub literally "If it doesn't match letters or spaces, convert it to nothing, process this variable."
Scrub extranous info or typos.
r marks it as a string literal (no escaping needed)
[] indicates a set of characters, special characters become normal.
^ = start of the string. In a set like this, it matches anything that is not in the string.
a-z matches lowercase letters.
Z-Z matches caps letters
 matches a space
+? essential means "try to match this till you can't"
More info: https://docs.python.org/3/library/re.html
'''

'''
### Stackoverflow thanks to:
First char of string: https://stackoverflow.com/questions/48973202/how-to-get-first-char-of-string-in-python
Length of string: https://stackoverflow.com/questions/4967580/how-to-get-the-size-of-a-string-in-python
Regex to scrub var of extranous info/symbols: https://stackoverflow.com/questions/44315941/regex-to-strip-all-numbers-and-special-characters-but-space-and-letters
Proper syntax for if blocks: https://stackoverflow.com/questions/37376516/python-check-if-multiple-variables-have-the-same-value
https://www.tutorialspoint.com/python/python_if_else.htm
Find substring in a string: https://stackoverflow.com/questions/3437059/does-python-have-a-string-contains-substring-method
'''

