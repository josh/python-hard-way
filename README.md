# Python Scripts

These are an assortment of my Python scripts, most of them while following "Python The Hard Way", some while not.

The most interesting (in my opinion) are [The Python Caeser Cipher Helper](Caeser-Cipher), [The Dots of Death (Use with caution)](3exercises/ex7-dots.py) and the latest is in Alpha state and is sadly lacking parameters which makes testing hard: [Sermon-Upload](Sermon-Upload)

You can also find out if you're a giant here (Read the comments): [Short and Tall](3exercises/ex11_Fun.py)
