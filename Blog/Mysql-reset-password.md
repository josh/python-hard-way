# Set/Reset MySQL Root Password on MySQL 14.14

I was trying to setup MySQL that I installed using `sudo apt install MySql` and couldn't remember how to set the root password. I found many different instructions on how to stop MySQL, boot into safe mode and reset the root password.

*None of them worked.*

This did:

Run: `sudo mysql_secure_installation`

You will see something like this:

```
Securing the MySQL server deployment.

Enter password for user root:
```

You can setup a password strength validation plugin if you wish in the next step:

```
VALIDATE PASSWORD PLUGIN can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD plugin?

Press y|Y for Yes, any other key for No:
```

Next, it will give you an option to disable anonymous users. Do it. It is a security liability if you don't.

```
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) :
```

Remote root login to MySQL is generally not a good thing. I recommend disabling it:

```
Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) :
```

Delete the test database to reduce the attack surface for hackers:

```
By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) :
```

Reload privilege tables now, unless you need MySQL working for this precise instant. It only takes a second.

```
Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) :
```

And you're done!
